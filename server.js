// Setup basic express server
require('dotenv').config();
var express = require('express');
var app = express();

const fs = require('fs');

var options = null;

if (process.env.SSL_ENABLE ==  'true') {
    options = {
        key: fs.readFileSync(process.env.SSL_KEY),
        cert: fs.readFileSync(process.env.SSL_CERT)
    };
    var server = require('https').createServer(options , app);
} else {
    var server = require('http').createServer(app);
}

var io = require('socket.io')(server);
var port = process.env.PORT || 3000;
var url = process.env.APP_URL;

app.get('/', function(req, res){
  res.send('server');
});

server.listen(port, function () {
  console.log('Server listening at port %d', port);
  console.log('app url %s', url);
});

// Routing
// app.use(express.static(path.join(__dirname, 'public')));

io.on('connection', function (socket) {

  // when the client emits 'new message', this listens and executes
  socket.on(url +'marco entrada', function (data) {
    // we tell the client to execute 'new message'
    socket.broadcast.emit(url +'marco entrada', {
      id: data
    });
    console.log('id: ' + data);
  });

  socket.on(url+'marco salida', function (data) {
    // we tell the client to execute 'new message'
    socket.broadcast.emit(url +'marco salida', {
      id: data
    });
    console.log('id: ' + data);
  });

  socket.on(url+'casco-mueve', function (data) {
    // we tell the client to execute 'new message'
    socket.broadcast.emit(url +'casco-mueve', {
      id: data
    });
    console.log('id: ' + data);
  });

});
